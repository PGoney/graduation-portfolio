import sys
import os
import xml.etree.ElementTree as ET
from operator import attrgetter

from my_data import SearchedSpectrum

def apply_fdr(sp_list, target_cnt, decoy_cnt, fdr_setting):
    sp_list.sort(key=attrgetter('score'))

    for i in range(len(sp_list)-1, -1, -1):
        sp = sp_list[i]

        fdr = decoy_cnt / target_cnt
        if fdr <= fdr_setting:
            threshold = sp.score
            break

        if sp.is_decoy():
            decoy_cnt -= 1
        else:
            target_cnt -= 1

        del(sp_list[i])

    sp_list.sort(key=attrgetter('index'))

    print("Threshold: %0.04f | FDR: %.04f%% | Target: %d \t| Decoy: %d" \
            % (threshold, fdr*100, target_cnt, decoy_cnt))

if __name__ == '__main__':
    file_in_list = sys.argv[1:-2]
    fdr_setting = float(sys.argv[-2])
    file_out = sys.argv[-1]

    for file_in in file_in_list:
        print("input: \t" + file_in)
    print("output:\t" + file_out)
    print("FDR:   \t%.02f%%" % (fdr_setting*100))
    print("--------------------------------")

    ns = '{http://regis-web.systemsbiology.net/pepXML}'

    sp_list = []

    index = 0
    decoy_cnt = 0
    target_cnt = 0

    for file_in in file_in_list:
        tree = ET.parse(file_in)
        msms_pipeline_analysis = tree.getroot()
        msms_run_summary = msms_pipeline_analysis.find(ns + 'msms_run_summary')

        for spectrum_query in msms_run_summary.findall(ns + 'spectrum_query'):
            try:
                search_result = spectrum_query.find(ns + 'search_result')
                search_hit = search_result.find(ns + 'search_hit')
                alternative_protein_list = search_hit.findall(ns + 'alternative_protein')
                search_score = None
                for search_score_iter in search_hit.findall(ns + 'search_score'):
                    if search_score_iter.attrib['name'] == 'expect':
                        search_score = search_score_iter
                        break
            except:
                continue

            title = spectrum_query.attrib['spectrumNativeID']
            scan_number = 0
            observed_mw = float(spectrum_query.attrib['precursor_neutral_mass'])
            charge = int(spectrum_query.attrib['assumed_charge'])
            calculated_mw = float(search_hit.attrib['calc_neutral_pep_mass'])
            delta_mass = float(search_hit.attrib['massdiff'])
            score = float(search_score.attrib['value'])

            peptide_prev_aa = search_hit.attrib['peptide_prev_aa']
            peptide_next_aa = search_hit.attrib['peptide_next_aa']
            peptide = peptide_prev_aa + '.' + search_hit.attrib['peptide'] + '.' + peptide_next_aa

            protein = [ search_hit.attrib['protein'] ]
            protein.extend([ p.attrib['protein'] for p in alternative_protein_list ])

            if len(peptide) < 6:
                continue

            index += 1
            print("\rsearched spectrum: " + str(index), end='')

            sp = SearchedSpectrum(title=title, index=index, scan_number=scan_number \
                    , observed_mw=observed_mw, charge=charge, calculated_mw=calculated_mw \
                    , delta_mass=delta_mass, score=score, peptide=peptide, protein=protein)
            sp_list.append(sp)

            if sp.is_decoy() == True:
                decoy_cnt += 1
            else:
                target_cnt += 1

    print('')

    apply_fdr(sp_list, target_cnt, decoy_cnt, fdr_setting)
    print("--------------------------------")

    with open(file_out, 'w') as f:
        f.write("title\tindex\tscan_number\tobserved_mw\tcharge\tcalculated_mw\t" \
                + "delta_mass\tscore\tpeptide\tprotein\n")
        for sp in sp_list:
            f.write(str(sp) + '\n')
