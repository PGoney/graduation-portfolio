import sys
import os

if __name__ == '__main__':
    file_in_list = sys.argv[1:-1]
    file_out = sys.argv[-1]

    try:
        if not os.path.exists(os.path.dirname(file_out)):
            os.mkdir(os.path.dirname(file_out))
    except:
        pass

    for file_in in file_in_list:
        print("Input: \t" + os.path.basename(file_in))
    print("Output:\t" + os.path.basename(file_out))

    f_out = open(file_out, 'w')

    cnt = 0
    for file_in in file_in_list:
        with open(file_in, 'r') as f_in:
            while True:
                line = f_in.readline()
                if not line: break

                f_out.write(line)

                if 'BEGIN' in line:
                    cnt += 1
                    print("\rspectrum count: " + str(cnt), end='')

    print('')
    print("--------------------------------")

    f_out.close()
