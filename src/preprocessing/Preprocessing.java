package preprocessing;

import java.util.ArrayList;
import java.util.Collections;

import preprocessing.object.Peak;
import preprocessing.object.FeatureManager;


public interface Preprocessing {
	void removeNoisePeak(FeatureManager sp_manager, String[] args);
	void permutation(FeatureManager sp_manager, String[] args);
	void scaling(FeatureManager sp_manager, String[] args);
	void imputation(FeatureManager sp_manager, String[] args);
	void zeroPadding(FeatureManager sp_manager, int size);
	void normalize(FeatureManager sp_manager, String[] args);
	void removeRedundancy(FeatureManager sp_manager);
}