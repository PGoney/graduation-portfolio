package preprocessing;

import java.util.ArrayList;
import java.util.Collections;

import preprocessing.object.FeatureManager;
import preprocessing.object.Peak;

public class HoJeongPreprocessing implements Preprocessing {

	@Override
	public void removeNoisePeak(FeatureManager sp_manager, String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public void permutation(FeatureManager sp_manager, String[] args) {
		// TODO Auto-generated method stub

	}

	public ArrayList<Peak> MinMax_Normalization(ArrayList<Peak> peaks, double min, double max) {

		ArrayList<Peak> normalized = new ArrayList<>();

		Peak min_peak, max_peak;

		min_peak = Collections.min(peaks);
		min_peak.intensity = min;
		max_peak = Collections.max(peaks);
		max_peak.intensity = max;

		for (int i = 0; i < peaks.size(); i++) {
			// double normalize_mz = (peaks.get(i).mz - min_peak.mz)/(max_peak.mz -
			// min_peak.mz); //Min-Max Normalization
			double peaks_mz = peaks.get(i).mz;
			double normalize_intensity = (peaks.get(i).intensity - min_peak.intensity)
					/ (max_peak.intensity - min_peak.intensity);
			// double peaks_intensity = peaks.get(i).intensity;
			Peak normalized_peak = new Peak(peaks_mz, normalize_intensity);
			normalized.add(normalized_peak);
		}

		return normalized;

	}

	public ArrayList<Peak> Standarization(ArrayList<Peak> peaks) {

		ArrayList<Peak> Standarized = new ArrayList<>();

		Peak mean_peak = new Peak(Mean_mz(peaks), Mean_intensity(peaks));

		for (int i = 0; i < peaks.size(); i++) {
			double standarized_mz = (peaks.get(i).mz - mean_peak.mz) / STD(peaks, 0);// 0 -> Calculating Standard
																						// Deviation of Population
			double standarized_inetensity = peaks.get(i).intensity;
			Peak Standarized_peak = new Peak(standarized_mz, standarized_inetensity);
			Standarized.add(Standarized_peak);
		}

		return Standarized;

	}

	public double Mean_mz(ArrayList<Peak> sample) {

		double sum = 0.0;

		for (int i = 0; i < sample.size(); i++)
			sum += sample.get(i).mz;

		return sum / sample.size();
	}

	public double Mean_intensity(ArrayList<Peak> sample) {

		double sum = 0.0;

		for (int i = 0; i < sample.size(); i++)
			sum += sample.get(i).intensity;

		return sum / sample.size();
	}

	public double STD(ArrayList<Peak> sample, int option) {
		if (sample.size() < 2)
			return Double.NaN;

		double sum = 0.0;
		double sd = 0.0;
		double diff;
		double Mean_mz = Mean_mz(sample);

		for (int i = 0; i < sample.size(); i++) {
			diff = sample.get(i).mz - Mean_mz;
			sum += diff * diff;
		}
		sd = Math.sqrt(sum / (sample.size() - option));

		return sd;
	}

	@Override
	public void imputation(FeatureManager sp_manager, String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public void zeroPadding(FeatureManager sp_manager, int size) {
		// TODO Auto-generated method stub

	}

	@Override
	public void normalize(FeatureManager sp_manager, String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeRedundancy(FeatureManager sp_manager) {
		// TODO Auto-generated method stub

	}

	@Override
	public void scaling(FeatureManager sp_manager, String[] args) {
		// TODO Auto-generated method stub
		
	}

}
