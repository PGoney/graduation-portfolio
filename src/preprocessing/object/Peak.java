package preprocessing.object;

/**
 * 
 * @author pjw23
 *
 */
public class Peak implements Comparable<Peak> {
	public double mz;
	public double intensity;
	
	public Peak(double mz, double intensity) {
		this.mz = mz;
		this.intensity = intensity;
	}
	
	public double getMass() {
		return mz;
	}
	
	public double getIntensity() {
		return intensity;
	}

	/**
	 * mz is sorted by ascending order.
	 */
	@Override
	public int compareTo(Peak p) {
		if (mz < p.mz)
			return -1;
		else if (mz > p.mz)
			return 1;
		else
			return 0;
	}
}