package preprocessing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import preprocessing.object.FeatureManager;
import preprocessing.object.Peak;

public class MyPreprocessing implements Preprocessing {

	@Override
	public void removeNoisePeak(FeatureManager sp_manager, String[] args) {
		// TODO Auto-generated method stub
		
		String[] METHOD_LIST = {
				"--TopN", "--TIC", "--masking"
		};
		String[][] PARAMETER_LIST = {
				{"-N", "150"}, {"--Da", ""}, {"--ppm", ""}
		};

		String method = METHOD_LIST[0];
		
		for (int i = 0; i < args.length; i++) {
			for (String METHOD : METHOD_LIST) {
				if (args[i].equals(METHOD))
					method = METHOD;
			}
			for (String[] PARAMETER : PARAMETER_LIST) {
				if (args[i].equals(PARAMETER[0]))
					PARAMETER[1] = args[++i];
			}
		}
		
		// TopN
		if (method.equals(METHOD_LIST[0])) {
			TopN(sp_manager, PARAMETER_LIST);
		}
	}

	private void TopN(FeatureManager sp_manager, String[][] PARAMETER_LIST) {
		int N = Integer.parseInt(PARAMETER_LIST[0][1]);
		double da = -1;
		double ppm = -1;
		
		if (!PARAMETER_LIST[1][1].equals(""))
			da = Double.parseDouble(PARAMETER_LIST[1][1]);
		if (!PARAMETER_LIST[2][1].equals(""))
			ppm = Double.parseDouble(PARAMETER_LIST[2][1]);
		
		for (int i = 0; i < sp_manager.size(); i++) {
			ArrayList<Peak> peak_list_origin = (ArrayList<Peak>) sp_manager.getPeakList(i);
			ArrayList<Peak> peak_list = new ArrayList<>();

			int cnt = 0;
			while (true) {
				if (cnt >= N) break;
				else if (peak_list_origin.size() == 0) break;
				
				double mz = getBasePeak(peak_list_origin).getMass();
				double tolerance = -1;
				
				if (da != -1)
					tolerance = da;
				else if (ppm != -1)
					tolerance = (ppm * mz * Math.pow(10, -6));
				
				Peak peak = binning(peak_list_origin, mz, tolerance);
				peak_list.add(peak);
				
				cnt++;
			}

			Collections.sort(peak_list);
			sp_manager.setPeakList(i, peak_list);
		}
	}
	
	private Peak getBasePeak(List<Peak> peak_list) {
		Peak base_peak = peak_list.get(0);
		
		for (Peak p : peak_list) {
			if (p.getIntensity() > base_peak.getIntensity())
				base_peak = p;
		}
		
		return base_peak;
	}
	
	private Peak binning(List<Peak> peak_list, double mz, double tolerance) {
		ArrayList<Peak> satisfied_peak_list = new ArrayList<>();
		
		for (Peak p : peak_list) {
			if (Math.abs(p.getMass() - mz) <= tolerance)
				satisfied_peak_list.add(p);
		}
		
		double weighed_mz = 0;
		double total_intensity = 0;
		for (Peak p : satisfied_peak_list) {
			weighed_mz += p.getMass() * p.getIntensity();
			total_intensity += p.getIntensity();
			peak_list.remove(p);
		}
		weighed_mz /= total_intensity;
		
		return new Peak(weighed_mz, total_intensity);
	}

	@Override
	public void permutation(FeatureManager sp_manager, String[] args) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void scaling(FeatureManager sp_manager, String[] args) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void imputation(FeatureManager sp_manager, String[] args) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void zeroPadding(FeatureManager sp_manager, int size) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void normalize(FeatureManager sp_manager, String[] args) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeRedundancy(FeatureManager sp_manager) {
		// TODO Auto-generated method stub
		
	}
	
}