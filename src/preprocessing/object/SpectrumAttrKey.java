package preprocessing.object;

public enum SpectrumAttrKey implements AttrKey {
	// Known
	TITLE(false),
	RTINSECONDS(false),
	SCANS(false),
	
	CHARGE(true),
	PRECURSOR_MASS(true),
	PRECURSOR_INTENSITY(true),
	
	// Need to calculate
	TIC(true),
	BASE_PEAK_MASS(true),
	BASE_PEAK_INTENSITY(true),
	NUM_OF_PEAKS(true),
	
	// ?
	MAX_SEQUENCE_TAG(true);
	
	private boolean isActive;
	
	SpectrumAttrKey(boolean isActive) {
		this.isActive = isActive;
	}
	
	public void active() {
		isActive = true;
	}
	
	public void disactive() {
		isActive = false;
	}
	
	public boolean isActive() {
		return isActive;
	}
}