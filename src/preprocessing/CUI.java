package preprocessing;

import java.util.Scanner;

public class CUI {
	
	public static String MSMS_file;
	public static String result_file;
	
	public static Scanner sc;
	
	public static final String CRLF = "\r\n";
	
	public CUI(String MSMS_file, String result_file) {
		CUI.MSMS_file = MSMS_file;
		CUI.result_file = result_file;
		
		sc = new Scanner(System.in);
	}
	
	public static void main(String[] args) {
		CUI cui = new CUI("test.mgf", "test.xml");
		cui.shell();
	}
	
	public void shell() {
		String input = null;
		
		try {
			do {
				showFile();
				
				System.out.print("prompt> ");
				input = sc.nextLine();
				
				if (excuteCommand(input.split(" ")) == 0)
					break;
				
			} while (!input.equals(""));
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
	
	private int excuteCommand(String[] args) {
		if (args.length == 0 || args[0].toUpperCase().equals("QUIT"))
			return Command.QUIT;

		switch (args[0]) {
		case "help":
			return Command.HELP.excute(args);
		case "feature":
			return Command.FEATURE.excute(args);
		case "write":
			return Command.WRITE.excute(args);
		}

		return Command.ERROR;
	}
	
	private void showFile() throws Exception {
		String line = "";
		
		if (MSMS_file == null && result_file == null)
			throw new Exception("Error: There is no file want to read.");

		line += "--------------------------------" + CRLF;
		if (MSMS_file != null)
			line += "MS/MS File: " + MSMS_file + CRLF;
		if (result_file != null)
			line += "Searched result file: " + result_file + CRLF;
		line += "--------------------------------" + CRLF;

		System.out.print(line);
	}
}