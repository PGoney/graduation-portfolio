package preprocessing.object;

public interface AttrKey {
	public void active();
	public void disactive();
	public boolean isActive();
}