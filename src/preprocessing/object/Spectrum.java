package preprocessing.object;

import java.util.ArrayList;
import java.util.List;

/**
 * Spectrum class stores data of tandem mass spectrometry (MS/MS).
 * 
 * @author pjw23
 *
 */
public class Spectrum extends Feature {	
	
	protected ArrayList<Peak> peak_list;

	/**
	 * MS/MS data file (*.mgf) has following structure.
	 * 
	 * BEGIN IONS
	 * TITLE=[title]
	 * PEPMASS=[precursor mass] [precursor intensity (skip)]
	 * CHARGE=[charge]
	 * RTINSECONS=[rtinseconds] (skip)
	 * SCANS=[scans] (skip)
	 * [mz] [intensity]
	 * [mz] [intensity]
	 * ...
	 * END IONS
	 * 
	 * These feature does not need calculation, so they are stored naively.
	 * 
	 * @param lines		Parsed MS/MS file (*.mgf)
	 */
	public Spectrum(String[] lines) {
		super();
		
		peak_list = new ArrayList<>();
		
		for (String line : lines) {
			if (line.equals("BEGIN IONS"))
				continue;
			else if (line.equals("END IONS"))
				break;
			
			if (line.contains("=")) {
				String[] split_line = line.split("=");
				String key = split_line[0];
				String value = split_line[1];
	
				switch (key) {
				case "TITLE":
					putAttr(SpectrumAttrKey.TITLE, value);
					break;
				case "RTINSECONDS":
					putAttr(SpectrumAttrKey.RTINSECONDS, Double.parseDouble(value));
					break;
				case "PEPMASS":
					if (value.contains(" ")) {
						String[] split_value = value.split(" ");
						double precursor_mass = Double.parseDouble(split_value[0]);
						double precursor_intensity = Double.parseDouble(split_value[1]);
						
						putAttr(SpectrumAttrKey.PRECURSOR_MASS, precursor_mass);
						putAttr(SpectrumAttrKey.PRECURSOR_INTENSITY, precursor_intensity);
					}
					
					// Precursor intensity data can be empty.
					else {
						double precursor_mass = Double.parseDouble(value);
	
						putAttr(SpectrumAttrKey.PRECURSOR_MASS, precursor_mass);
					}
					break;
				case "CHARGE":
					putAttr(SpectrumAttrKey.CHARGE, Double.parseDouble(value));
				case "SCANS":
					putAttr(SpectrumAttrKey.SCANS, Integer.parseInt(value));
					break;
				}
			}
			else {
				String[] split_line = line.split(" ");
				double mz = Double.parseDouble(split_line[0]);
				double intensity = Double.parseDouble(split_line[1]);
				peak_list.add(new Peak(mz, intensity));
			}
		}
	}
	
	/**
	 * Some feature need to calculate.
	 * If any feature is not needed to user, then it does not do calculation. 
	 * In this class, basically,
	 * 		they are TIC, # of peaks, base peak mass/intensity and max sequence tag.
	 * 
	 * @throws Exception	There is no peak list or method related to feature.
	 */
	public void generateAttr() throws Exception {
		if (peak_list.isEmpty())
			throw new Exception("Error: There is no peak list.");
		
		SpectrumAttrKey[] key_set = SpectrumAttrKey.values();

		Peak base_peak = null;
		
		for (SpectrumAttrKey key : key_set) {
			// key.isActive == false	, Unnecessary feature.
			// getAttr(key) == null		, Already know value of feature (ex. charge). 
			if (!key.isActive() || getAttr(key) != null)
				continue;
			
			if (key == SpectrumAttrKey.TIC)
				putAttr(SpectrumAttrKey.TIC, TIC());
			else if (key == SpectrumAttrKey.NUM_OF_PEAKS)
				putAttr(SpectrumAttrKey.NUM_OF_PEAKS, numOfPeaks());
			else if (key == SpectrumAttrKey.MAX_SEQUENCE_TAG)
				putAttr(SpectrumAttrKey.MAX_SEQUENCE_TAG, getMaxSequenceTag());
			
			else if (key == SpectrumAttrKey.BASE_PEAK_MASS) {
				if (base_peak == null)
					base_peak = BasePeak();
				putAttr(SpectrumAttrKey.BASE_PEAK_MASS, base_peak.mz);
			}
			else if (key == SpectrumAttrKey.BASE_PEAK_INTENSITY) {
				if (base_peak == null)
					base_peak = BasePeak();
				putAttr(SpectrumAttrKey.BASE_PEAK_INTENSITY, base_peak.intensity);
			}
			
			// When this exception is raised, then register the method related to feature at this function like above feature.
			else
				throw new Exception("Error: There is unregistered method of feature.");
		}
	}
	
	public List<Peak> getPeakList() {
		return peak_list;
	}
	
	public void setPeakList(List<Peak> peak_list) {
		this.peak_list = (ArrayList<Peak>) peak_list;
	}
	
	/**
	 * Most abundant peak
	 * @return
	 */
	private Peak BasePeak() {
		Peak base_peak = peak_list.get(0);

		for (Peak p : peak_list)
			if (base_peak.intensity < p.intensity)
				base_peak = p;
		
		return base_peak;
	}
	
	/**
	 * The total ion current
	 * @return
	 */
	private double TIC() {
		double tic = 0;
		
		for (Peak p : peak_list)
			tic += p.intensity;
		
		return tic;
	}
	
	/**
	 * # of peaks.
	 * @return
	 */
	private int numOfPeaks() {
		return peak_list.size();
	}
	
	/**
	 * ???
	 * @return
	 */
	private Object getMaxSequenceTag() {
		return null;
	}
}