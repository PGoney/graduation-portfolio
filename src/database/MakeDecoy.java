package database;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Random;


public class MakeDecoy {
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
        String file_in = args[0];
        String file_out = file_in.substring(0, file_in.lastIndexOf(".")) + ".revCat" + file_in.substring(file_in.lastIndexOf("."), file_in.length());

		String prefix = "decoy";

		FileReader fileReader = new FileReader(args[0]);
		PrintWriter err = new PrintWriter(new BufferedWriter(new FileWriter(file_out)));

		BufferedReader bufferedReader = new BufferedReader(fileReader);
		String s;
		String Protein = new String();
		Date date = new Date();
		Random r = new Random();
		
		
		// reverse : 1  shuffle : 2
		int decoy_option = 1;
		
		while((s = bufferedReader.readLine()) != null) 
		{
			if( s.startsWith(">") )
			{
				if(Protein.compareTo("") != 0)
				{
					char[] Suffle = new char[Protein.length()];
					int[] check = new int[Protein.length()];
					for(int i=0;i<Protein.length();i++) check[i] = 0;
					
					int start=0, end=0, tmp=0;				
					for(int i=0;i<Protein.length();i++)
					{
						if( Protein.charAt(i) == 'K' || Protein.charAt(i) == 'R' ) {
							Suffle[i] = Protein.charAt(i);
							check[i] = 1;
							tmp = i;
							if(tmp == 0) 
								start = 1;
						}
						if(tmp != end) 
						{
							end = tmp;
							int occ = 0;
							if( decoy_option==1 )	/* REVERSE */
							{
								for(int j=end-1;j>=start;j--) {
									Suffle[occ+start] = Protein.charAt(j);
									occ++;
								}
							}
							if( decoy_option==2 )	/* SUFFLE */
							{
								while(occ < end-start)
								{
									int random = r.nextInt(end-start)+start;
									if(check[random] != 1)
									{
										check[random] = 1;
										Suffle[occ+start] = Protein.charAt(random);
										occ++;
										//occupy++;
									}
								}
							}
							start = tmp+1;
						}
					}
					if(tmp != Protein.length())
					{
						end = Protein.length();
						int occ = 0;
						if( decoy_option==1 )	/* REVERSE */
						{
							for(int j=end-1;j>=start;j--) {
								Suffle[occ+start] = Protein.charAt(j);
								occ++;
							}
						}
						if( decoy_option==2 )	/* SUFFLE */
						{
							while(occ < end-start)
							{
								int random = r.nextInt(end-start)+start;
								if(check[random] != 1)
								{
									check[random] = 1;
									Suffle[occ+start] = Protein.charAt(random);
									occ++;
									//occupy++;
								}
							}
						}
						start = tmp+1;
					}
					err.println(Suffle);
//					err.println();
					Protein = new String();
				}
				String tmp = ">" + prefix + "_";
				tmp = tmp.concat(s.substring(1));
				err.println(tmp);
			}
			else 
				Protein = Protein.concat(s);
		}
		if(Protein.compareTo("") != 0) {
			char[] Suffle = new char[Protein.length()];
			int[] check = new int[Protein.length()];
			for(int i=0;i<Protein.length();i++) check[i] = 0;
			
			int start=0, end=0, tmp=0;				
			for(int i=0;i<Protein.length();i++) {
				if( Protein.charAt(i) == 'K' || Protein.charAt(i) == 'R' ) {
					Suffle[i] = Protein.charAt(i);
					check[i] = 1;
					tmp = i;
					if(tmp == 0) start = 1;
				}
				if(tmp != end) {
					end = tmp;
					int occ = 0;
					
					/* REVERSE */
					if( decoy_option==1 )	/* REVERSE */
						{
						for(int j=end-1;j>=start;j--) {
							Suffle[occ+start] = Protein.charAt(j);
							occ++;
						}
						}
					/* SUFFLE */
					if( decoy_option==2 )	/* REVERSE */
					{
						while(occ < end-start){
							int random = r.nextInt(end-start)+start;
							if(check[random] != 1) {
								check[random] = 1;
								Suffle[occ+start] = Protein.charAt(random);
								occ++;
								//occupy++;
							}
						}
					}
					start = tmp+1;
				}
			}
			if(tmp != Protein.length()) {
				end = Protein.length();
				int occ = 0;
				if( decoy_option==1 )	/* REVERSE */
					{
					for(int j=end-1;j>=start;j--) {
						Suffle[occ+start] = Protein.charAt(j);
						occ++;
					}
				}
				if( decoy_option==1 )	/* REVERSE */
					{
					while(occ < end-start){
						int random = r.nextInt(end-start)+start;
						if(check[random] != 1) {
							check[random] = 1;
							Suffle[occ+start] = Protein.charAt(random);
							occ++;
							//occupy++;
						}
					}
				}			
				start = tmp+1;
			}
			err.println(Suffle);
//			err.println();
			Protein = new String();
		}
		err.close();
//		System.out.println("END");
	}

}
