import sys

from my_data import Protein, ENZYME, HOWTO_DECOY

class Database:
    DECOY_PREFIX = "decoy_"

    def __init__(self):
        self.protein_list = []

    def __repr__(self):
        line = ""

        line += "Protein information" + '\n'
        line += "Count: " + str(len(self.protein_list))

        return line

    def load(self, file_in):
        with open(file_in, 'r') as f:
            information = None
            sequence = ""

            while True:
                line = f.readline()
                if not line:
                    protein = Protein(information, sequence)
                    self.add_row(protein)
                    break
                line = line.rstrip()

                try:
                    if line[0] == '>':
                        if information != None:
                            protein = Protein(information, sequence)
                            self.add_row(protein)

                            information = None
                            sequence = ""

                        information = line[1:]
                    else:
                        sequence += line
                except:
                    pass

    def add_row(self, protein):
        self.protein_list.append(protein)

    def save(self, file_out):
        with open(file_out, 'w') as f:
            for protein in self.protein_list:
                f.write(str(protein) + '\n')

    def extend(self, db):
        self.protein_list.extend(db.protein_list)

    def make_decoy(self, amino_ch_list=ENZYME.TRYPSIN.value, howto=HOWTO_DECOY.REVERSE):
        decoy_db = Database()

        for protein in self.protein_list:
            protein = protein.make_decoy(amino_ch_list, howto)
            protein.add_prefix(self.DECOY_PREFIX)
            decoy_db.add_row(protein)

        return decoy_db

if __name__ == "__main__":
    file_in_list = sys.argv[1:-1]
    file_out = sys.argv[-1]

    all_db = Database()

    db_list = []
    decoy_db_list = []

    for file_in in file_in_list:
        db = Database()
        db.load(file_in)
        db_list.append(db)
        decoy_db = db.make_decoy()
        decoy_db_list.append(decoy_db)

    for db in db_list:
        all_db.extend(db)

    for db in decoy_db_list:
        all_db.extend(db)

    all_db.save(file_out)
