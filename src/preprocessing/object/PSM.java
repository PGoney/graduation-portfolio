package preprocessing.object;

import java.util.List;

/**
 * PSM class holds data about searched result data.
 * 
 * @author pjw23
 *
 */
public class PSM extends Feature {
	
	/**
	 * There are various structure of result file depended on searching program such comet-ms or moda.
	 * We parse it, and it will has following structure.
	 * 
	 * [feature name]=[feature value]
	 * 
	 * These expressed feature does not need calculation.
	 * 
	 * @param lines
	 */
	public PSM(String[] lines) {
		super();
		
		for (String line : lines) {
			String[] split_line = line.split("=");
			String key = split_line[0];
			String value = split_line[1];
			
			switch(key) {
			case "protein":
				putAttr(PSMAttrKey.PROTEIN, value);
				break;
			case "peptide":
				putAttr(PSMAttrKey.PEPTIDE, value);
				break;
			case "calculated_neutral_pep_mass":
				putAttr(PSMAttrKey.CALCULATED_NEUTRAL_PEP_MASS, Double.parseDouble(value));
				break;
			case "delta_mass":
			case "massdiff":
				putAttr(PSMAttrKey.DELTA_MASS, Double.parseDouble(value));
				break;
			case "peptide_sequence_length":
				putAttr(PSMAttrKey.PEPTIDE_SEQUENCE_LENGTH, Integer.parseInt(value));
				break;
			case "search_score":
				putAttr(PSMAttrKey.SEARCH_SCORE, Double.parseDouble(value));
				break;
			case "num_of_missed_cleavage":
				putAttr(PSMAttrKey.NUM_OF_MISSED_CLEAVAGE, Integer.parseInt(value));
				break;
			case "num_of_modifications":
				putAttr(PSMAttrKey.NUM_OF_MODIFICATIONS, Integer.parseInt(value));
				break;
			case "ntt":
				putAttr(PSMAttrKey.NTT, Integer.parseInt(value));
				break;
			}
		}
	}
	
	/**
	 * Some feature need to calculate.
	 * If any feature is not needed to user, then it does not do calculation. 
	 * In this class, basically,
	 * 		they are matched mass error mean/std,
	 *			matched b/y ion fraction,
	 * 			consecutive b/y ion fraction,
	 * 			NTT (can be expressed at result file).
	 * 
	 * @throws Exception	method related to feature.
	 */
	public void generateAttr() throws Exception {
		PSMAttrKey[] key_set = PSMAttrKey.values();
		
		for (PSMAttrKey key : key_set) {
			// key.isActive == false	, Unnecessary feature.
			// getAttr(key) == null		, Already know value of feature (ex. delta mass). 
			if (!key.isActive() || getAttr(key) != null)
				continue;

			if (key == PSMAttrKey.MATCHED_MASS_ERROR_MEAN)
				putAttr(PSMAttrKey.MATCHED_MASS_ERROR_MEAN, matchedMassErrorMean());
			else if (key == PSMAttrKey.MATCHED_MASS_ERROR_STD)
				putAttr(PSMAttrKey.MATCHED_MASS_ERROR_STD, matchedMassErrorStd());
			else if (key == PSMAttrKey.NTT)
				putAttr(PSMAttrKey.NTT, ntt());
			else if (key == PSMAttrKey.MATCHED_B_ION_FRACTION)
				putAttr(PSMAttrKey.MATCHED_B_ION_FRACTION, matchedBIonFraction());
			else if (key == PSMAttrKey.MATCHED_Y_ION_FRACTION)
				putAttr(PSMAttrKey.MATCHED_Y_ION_FRACTION, matchedYIonFraction());
			else if (key == PSMAttrKey.CONSECUTIVE_B_ION_FRACTION)
				putAttr(PSMAttrKey.CONSECUTIVE_B_ION_FRACTION, consecutiveBIonFraction());
			else if (key == PSMAttrKey.CONSECUTIVE_Y_ION_FRACTION)
				putAttr(PSMAttrKey.CONSECUTIVE_Y_ION_FRACTION, consecutiveYIonFraction());
			else
				throw new Exception("Error: There is unregistered method of feature.");
		}
	}
	
	private double matchedMassErrorMean() {
		return 0;
	}
	
	private double matchedMassErrorStd() {
		return 0;
	}
	
	private int ntt() {
		return 0;
	}
	
	private Object matchedBIonFraction() {
		return null;
	}
	
	private Object matchedYIonFraction() {
		return null;
	}
	
	private Object consecutiveBIonFraction() {
		return null;
	}
	
	private Object consecutiveYIonFraction() {
		return null;
	}

	@Override
	public List<Peak> getPeakList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setPeakList(List<Peak> peak_list) {
		// TODO Auto-generated method stub
		
	}
}
