package preprocessing.file;

import java.util.ArrayList;

import preprocessing.object.FeatureManager;

public interface ProteomicsFile {
	void read(FeatureManager sp_manager, String[] args);

	String write(FeatureManager sp_manager, String[] args);

	ArrayList<String[]> parsing(String file_name);
}