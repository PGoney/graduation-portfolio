package preprocessing.object;

import java.util.List;

public class MyFeatureManager implements FeatureManager {

	@Override
	public void add(String[] lines) {
		// TODO Auto-generated method stub
		
		if (lines[0].equals("MS/MS")) {
			Feature[] spectrum = { new Spectrum(lines), null };
			
			spectrum_list.add(spectrum);
		}
	}

	@Override
	public Feature[] remove(int index) {
		// TODO Auto-generated method stub
		return spectrum_list.remove(index);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return spectrum_list.size();
	}

	@Override
	public List<Peak> getPeakList(int index) {
		// TODO Auto-generated method stub
		
		Feature[] spectrum = spectrum_list.get(index);
		
		return spectrum[0].getPeakList();
	}

	@Override
	public void setPeakList(int index, List<Peak> peak_list) {
		// TODO Auto-generated method stub
		
		Feature[] spectrum = spectrum_list.get(index);
		
		spectrum[0].setPeakList(peak_list);
	}

	@Override
	public void generateAttr()  {
		// TODO Auto-generated method stub
		
		try {
			for (Feature[] spectrum : spectrum_list) {
				spectrum[0].generateAttr();
				spectrum[1].generateAttr();
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

}
