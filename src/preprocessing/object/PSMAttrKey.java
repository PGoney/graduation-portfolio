package preprocessing.object;

public enum PSMAttrKey implements AttrKey {
	// Known
	PROTEIN(false),
	PEPTIDE(false),
	
	CALCULATED_NEUTRAL_PEP_MASS(true),
	DELTA_MASS(true),
	PEPTIDE_SEQUENCE_LENGTH(true),
	SEARCH_SCORE(true),
	NUM_OF_MISSED_CLEAVAGE(true),
	NUM_OF_MODIFICATIONS(true),
	TARGET_DECOY_LABEL(true),
	
	// Need to calculate
	MATCHED_MASS_ERROR_MEAN(true),
	MATCHED_MASS_ERROR_STD(true),
	NTT(true),
	
	// ?
	MATCHED_B_ION_FRACTION(true),
	MATCHED_Y_ION_FRACTION(true),
	CONSECUTIVE_B_ION_FRACTION(true),
	CONSECUTIVE_Y_ION_FRACTION(true);
	
	private boolean isActive;
	
	PSMAttrKey(boolean isActive) {
		this.isActive = isActive;
	}
	
	public void active() {
		isActive = true;
	}
	
	public void disactive() {
		isActive = false;
	}
	
	public boolean isActive() {
		return isActive;
	}
}