CLASSES_DIR := bin
SRC_DIR := src

JAVAC = javac
JAVA = java

JFLAGS = -g -d
RM = rm -r

.SUFFIXES: .java .class

.java.class:
	$(JAVAC) $(JFLAGS) $(CLASSES_DIR) $*.java

CLASSES = \
          $(SRC_DIR)/database/*.java

default: classes

classes: $(CLASSES:.java=.class)

clean:
	$(RM) $(CLASSES_DIR)/database
