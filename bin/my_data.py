import enum
from operator import attrgetter

class HOWTO_DECOY(enum.Enum):
    REVERSE = 0

class ENZYME(enum.Enum):
    TRYPSIN = "RK"

class Protein:
    def __init__(self, information, sequence):
        self.information = information
        self.sequence = sequence

    def __str__(self):
        line = ">" + self.information + '\n'
        line += self.sequence

        return line

    def add_prefix(self, prefix):
        self.information = prefix + self.information

    def make_decoy(self, amino_ch_list=ENZYME.TRYPSIN.value, howto=HOWTO_DECOY.REVERSE):
        decoy_sequence = ""

        if howto == HOWTO_DECOY.REVERSE:
            partial_sequence = ""
            for ch in self.sequence:
                if ch not in amino_ch_list:
                    partial_sequence += ch
                    continue
                else:
                    decoy_sequence += partial_sequence[::-1]
                    decoy_sequence += ch
                    partial_sequence = ""
            decoy_sequence += partial_sequence[::-1]

            return Protein(self.information, decoy_sequence)

class Peak:
    def __init__(self, mz, intensity=None):
        self.mz = mz
        self.intensity = intensity

    def __str__(self):
        if self.intensity == None:
            return "%.06f" % (self.mz)
        return "%.06f %.06f" % (self.mz, self.intensity)

class Spectra:
    def __init__(self):
        self.title = None
        self.rtinseconds = None
        self.pepmass = None
        self.charge = None
        self.scans = None
        self.peak_list = []

    def __str__(self):
        line = "BEGIN IONS" + '\n'
        line += "TITLE=" + self.title + '\n'
        if self.rtinseconds != None:
            line += "RTINSECONDS=%.04f\n" % (self.rtinseconds)
        if self.pepmass != None:
            line += "PEPMASS=" + str(self.pepmass) + '\n'
        if self.charge != None:
            line += "CHARGE=" + str(self.charge) + '+' + '\n'
        if self.scans != None:
            line += "SCANS=" + str(self.scans) + '\n'
        for peak in self.peak_list:
            line += str(peak) + '\n'
        line += "END IONS"

        return line

    def load(self, lines):
        peak_list = []

        for line in lines:
            if 'TITLE' in line:
                index = line.find('=')
                self.title = line[index+1:]
            elif 'RTINSECONDS' in line:
                index = line.find('=')
                self.rtinseconds = float(line[index+1:])
            elif 'PEPMASS' in line:
                index = line.find('=')
                line = line[index+1:]
                split_line = line.split()
                try:
                    mz = float(split_line[0])
                    intensity = float(split_line[1])
                except:
                    intensity =  None
                self.pepmass = Peak(mz, intensity)
            elif 'CHARGE' in line:
                index = line.find('=')
                line = line[index+1:]
                split_line = line.split('+')
                self.charge = int(split_line[0])
            elif 'SCANS' in line:
                index = line.find('=')
                self.scans = int(line[index+1:])
            else:
                split_line = line.split()
                mz = float(split_line[0])
                intensity = float(split_line[1])
                peak_list.append(Peak(mz, intensity))
        self.peak_list = peak_list

    @staticmethod
    def __get_max_intensity_peak(peak_list):
        max_index = 0
        for i in range(1, len(peak_list)):
            if peak_list[max_index].intensity < peak_list[i].intensity:
                max_index = i
        return peak_list[max_index]

    @staticmethod
    def __binning(peak_list, mz, tolerance):
        satisfied_peak_list = [ peak for peak in peak_list if abs(peak.mz - mz) <= tolerance ]

        weighted_mz = 0
        total_intensity = 0
        for peak in satisfied_peak_list:
            weighted_mz += peak.mz * peak.intensity
            total_intensity += peak.intensity
            peak_list.remove(peak)
        weighted_mz /= total_intensity

        return Peak(weighted_mz, total_intensity)

    def binning(self, N, ppm=None, da=None):
        copy_peak_list = self.peak_list
        self.peak_list = []

        cnt = 0
        while True:
            if cnt >= N: break
            elif len(copy_peak_list) == 0: break

            mz = Spectra.__get_max_intensity_peak(copy_peak_list).mz

            if ppm != None:
                tolerance = (ppm * mz * (10 ** -6))
            elif da != None:
                pass
            else:
                raise Exception()

            peak = Spectra.__binning(copy_peak_list, mz, tolerance)
            self.peak_list.append(peak)

            cnt += 1

        self.peak_list.sort(key=attrgetter("mz"))

class SearchedSpectrum:
    def __init__(self, **kwargs):
        self.title = kwargs.get('title')
        self.index = kwargs['index']
        self.scan_number = kwargs['scan_number']
        self.observed_mw = kwargs['observed_mw']
        self.charge = kwargs['charge']
        self.calculated_mw = kwargs['calculated_mw']
        self.delta_mass = kwargs['delta_mass']
        self.score = kwargs['score']
        self.peptide = kwargs['peptide']
        self.protein = kwargs['protein']

        self.spectrum_file = kwargs.get('spectrum_file')
        self.probability = kwargs.get('probability')
        self.peptide_position = kwargs.get('peptide_position')

    def __str__(self):
        line = ''

        if self.title != None:
            line += self.title + '\t'
        elif self.spectrum_file != None:
            line += self.spectrum_file + '\t'

        line += str(self.index) + '\t'
        line += str(self.scan_number) + '\t'
        line += str(self.observed_mw) + '\t'
        line += str(self.charge) + '\t'
        line += str(self.calculated_mw) + '\t'
        line += str(self.delta_mass) + '\t'
        line += str(self.score) + '\t'

        if self.probability != None:
            line += str(self.probability) + '\t'

        line += self.peptide + '\t'
        line += self.get_protein()

        if self.peptide_position != None:
            line += self.peptide_position + '\t'

        return line

    def is_decoy(self, prefix='decoy_'):
        for p in self.protein:
            if prefix not in p:
                return False
        return True

    def get_protein(self, prefix='decoy_'):
        for p in self.protein:
            if prefix not in p:
                return p
        return self.protein[0]
