import sys
import os

from my_data import Spectra
from my_data import Peak

if __name__ == '__main__':
    option = sys.argv[1]
    file_in = sys.argv[2]
    ppm_or_da = sys.argv[3]
    N = int(sys.argv[4])

    ppm = None
    da = None
    if 'ppm' in ppm_or_da:
        ppm = float(ppm_or_da.split('ppm')[0])
    elif 'Da' in ppm_or_da:
        da = float(ppm_or_da.split('Da')[0])

    dir_name = os.path.dirname(file_in) + '/binning_' + str(N)
    base_name = os.path.basename(file_in)
    file_name = os.path.splitext(base_name)[0] + '_binning_' + str(N)
    ext_name = os.path.splitext(base_name)[1]

    if not os.path.exists(dir_name):
        os.mkdir(dir_name)

    file_out = os.path.join(dir_name, file_name + ext_name)

    print("Method:   \t" + option)
    print("Input:    \t" + os.path.basename(file_in))
    print("Output:   \t" + file_name + ext_name)
    print("ppm or Da:\t" + ppm_or_da)
    print("N:        \t" + str(N))

    f_out = open(file_out, 'w')
    f_in = open(file_in, 'r')

    cnt = 1
    while True:
        line = f_in.readline()
        if not line: break
        line = line.strip()

        if line == 'BEGIN IONS':
            lines = []
            continue
        elif line == 'END IONS':
            spectra = Spectra()
            spectra.load(lines)

            if option == '--binning':
                spectra.binning(N, ppm=ppm, da=da)
            else:
                raise Exception()

            f_out.write(str(spectra))
            f_out.write('\n')

            print("\rspectrum count: " + str(cnt), end='')
            cnt += 1

            continue
        lines.append(line)
    print('')
    print("--------------------------------")

    f_in.close()
    f_out.close()
