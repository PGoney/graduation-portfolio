package preprocessing;

import java.util.ArrayList;
import java.util.Scanner;

import preprocessing.object.PSMAttrKey;
import preprocessing.object.SpectrumAttrKey;

public abstract class Command {
	public static final int QUIT = 0;
	public static final int NORMAL = 1;
	public static final int ERROR = -1;
	
	public static final String CRLF = "\r\n";
	
	public static final Command HELP = new Help();
	public static final Command FEATURE = new Feature();
	public static final Command WRITE = new Write();
	
	protected String[] usage;
	protected String[][] options;
	
	public String getShortHelp() {
		return String.format("%-16s\t%s", usage[0], usage[1]);
	}
	
	public void printHelp() {
		String line = "";
		
		line += getShortHelp() + CRLF + CRLF;
		line += String.format("%-16s\t%s", "[USAGE]", "[EXPLAIN]") + CRLF;
		for (String[] option : options)
			line += String.format("%-16s\t%s", option[0], option[1]) + CRLF;

		System.out.println(line);
	}
	
	public abstract int excute(String[] args);
}

class Help extends Command {
	
	Help() {
		String[] temp_usage = { "help", "It offers information of each instruction" };
		String[][] temp_options = {};
		
		usage = temp_usage;
		options = temp_options;
	}

	@Override
	public int excute(String[] args) {
		// TODO Auto-generated method stub

		String line = "";
		
		line += String.format("%-16s\t%s", "[USAGE]", "[EXPLAIN]") + CRLF;
		line += Command.HELP.getShortHelp() + CRLF;
		line += Command.FEATURE.getShortHelp() + CRLF;
		line += Command.WRITE.getShortHelp() + CRLF;
		
		System.out.print(line);
		
		return NORMAL;
	}
}

class Feature extends Command {
	
	Feature() {
		String[] temp_usage = { "feature", "Print feature list" };
		String[][] temp_options = {
				{ "--help", "It offers information of command" },
				{ "-a", "Print active feature list" },
				{ "-d", "Print disactive feature list" },
				{ "-m", "Manage feature list" }
		};
		
		usage = temp_usage;
		options = temp_options;
	}

	@Override
	public int excute(String[] args) {
		// TODO Auto-generated method stub
		
		if (args.length == 1) {
			printFeature();
			return NORMAL;
		}
		
		switch (args[1]) {
		case "--help":
			printHelp();
			return NORMAL;
		case "-a":
			printActiveFeature();
			return NORMAL;
		case "-d":
			printDisactiveFeature();
			return NORMAL;
		case "-m":
			manageFeature();
			return NORMAL;
		}
		
		return ERROR;
	}
	
	private void manageFeature() {
		// TODO Auto-generated method stub
		
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		String input = null;
		
		System.out.print("Active: ");
		input = sc.nextLine().toUpperCase();
		
		String[] active_feature_list = input.split(" ");
				
		System.out.print("Disactive: ");
		input = sc.nextLine().toUpperCase();
		
		String[] disactive_feature_list = input.split(" ");
		
		for (String active_feature : active_feature_list) {
			for (SpectrumAttrKey key : SpectrumAttrKey.values()) {
				if (key.name().equals(active_feature))
					key.active();
			}
			for (PSMAttrKey key : PSMAttrKey.values()) {
				if (key.name().equals(active_feature))
					key.active();
			}
		}
		for (String disactive_feature : disactive_feature_list) {
			for (SpectrumAttrKey key : SpectrumAttrKey.values()) {
				if (key.name().equals(disactive_feature))
					key.disactive();
			}
			for (PSMAttrKey key : PSMAttrKey.values()) {
				if (key.name().equals(disactive_feature))
					key.disactive();
			}
		}
	}

	private void printDisactiveFeature() {
		// TODO Auto-generated method stub

		String line = "";
		
		ArrayList<SpectrumAttrKey> sp_feature_list = new ArrayList<SpectrumAttrKey>();
		ArrayList<PSMAttrKey> psm_feature_list = new ArrayList<PSMAttrKey>();
		
		for (SpectrumAttrKey key : SpectrumAttrKey.values())
			if (!key.isActive())
				sp_feature_list.add(key);
		
		for (PSMAttrKey key : PSMAttrKey.values())
			if (!key.isActive())
				psm_feature_list.add(key);
		
		line += String.format("%-24s\t%-8s\t%-24s\t%-8s", "[SPECTRUM]", "[ACTIVE]", "[PSM]", "[ACTIVE]") + CRLF + CRLF;
				
		int index = 0;
		while (true) {
			if (index >= sp_feature_list.size() && index >= psm_feature_list.size())
				break;
			
			String sp_feature = "";
			String psm_feature = "";
			
			String sp_isActive = "";
			String psm_isActive = "";
			
			if (index < sp_feature_list.size()) {
				sp_feature = sp_feature_list.get(index).name();
				sp_isActive = String.valueOf(sp_feature_list.get(index).isActive());
			}
			if (index < psm_feature_list.size()) {
				psm_feature = psm_feature_list.get(index).name();
				psm_isActive = String.valueOf(psm_feature_list.get(index).isActive());
			}
			
			line += String.format("%-24s\t%-8s\t%-24s\t%-8s", sp_feature, sp_isActive, psm_feature, psm_isActive) + CRLF;
			
			index++;
		}
		
		System.out.print(line);
	}

	private void printActiveFeature() {
		// TODO Auto-generated method stub

		String line = "";
		
		ArrayList<SpectrumAttrKey> sp_feature_list = new ArrayList<SpectrumAttrKey>();
		ArrayList<PSMAttrKey> psm_feature_list = new ArrayList<PSMAttrKey>();
		
		for (SpectrumAttrKey key : SpectrumAttrKey.values())
			if (key.isActive())
				sp_feature_list.add(key);
		
		for (PSMAttrKey key : PSMAttrKey.values())
			if (key.isActive())
				psm_feature_list.add(key);
		
		line += String.format("%-24s\t%-8s\t%-24s\t%-8s", "[SPECTRUM]", "[ACTIVE]", "[PSM]", "[ACTIVE]") + CRLF;
				
		int index = 0;
		while (true) {
			if (index >= sp_feature_list.size() && index >= psm_feature_list.size())
				break;
			
			String sp_feature = "";
			String psm_feature = "";
			
			String sp_isActive = "";
			String psm_isActive = "";
			
			if (index < sp_feature_list.size()) {
				sp_feature = sp_feature_list.get(index).name();
				sp_isActive = String.valueOf(sp_feature_list.get(index).isActive());
			}
			if (index < psm_feature_list.size()) {
				psm_feature = psm_feature_list.get(index).name();
				psm_isActive = String.valueOf(psm_feature_list.get(index).isActive());
			}
			
			line += String.format("%-24s\t%-8s\t%-24s\t%-8s", sp_feature, sp_isActive, psm_feature, psm_isActive) + CRLF;
			
			index++;
		}
		
		System.out.print(line);
	}

	private void printFeature() {
		String line = "";
		
		SpectrumAttrKey[] sp_feature_list = SpectrumAttrKey.values();
		PSMAttrKey[] psm_feature_list = PSMAttrKey.values();
		
		line += String.format("%-24s\t%-8s\t%-24s\t%-8s", "[SPECTRUM]", "[ACTIVE]", "[PSM]", "[ACTIVE]") + CRLF;
		
		int index = 0;
		while (true) {
			if (index >= sp_feature_list.length && index >= psm_feature_list.length)
				break;
			
			String sp_feature = "";
			String psm_feature = "";
			
			String sp_isActive = "";
			String psm_isActive = "";
			
			if (index < sp_feature_list.length) {
				sp_feature = sp_feature_list[index].name();
				sp_isActive = String.valueOf(sp_feature_list[index].isActive());
			}
			if (index < psm_feature_list.length) {
				psm_feature = psm_feature_list[index].name();
				psm_isActive = String.valueOf(psm_feature_list[index].isActive());
			}
			
			line += String.format("%-24s\t%-8s\t%-24s\t%-8s", sp_feature, sp_isActive, psm_feature, psm_isActive) + CRLF;
			
			index++;
		}
		
		System.out.print(line);
	}
}

class Write extends Command {

	Write() {
		String[] temp_usage = { "write", "Write features at file" };
		String[][] temp_options = {
				{ "--help", "It offers information of command" }
		};
		
		usage = temp_usage;
		options = temp_options;
	}
	
	@Override
	public int excute(String[] args) {
		// TODO Auto-generated method stub
		
		if (args.length > 1) {
			
			switch(args[1]) {
			case "--help":
				printHelp();
				return NORMAL;
			}
		}
		
		return NORMAL;
	}
	
}