package preprocessing.file;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;

import preprocessing.object.FeatureManager;
import preprocessing.object.Peak;

public class HoJeongFile implements ProteomicsFile {

	@Override
	public void read(FeatureManager sp_manager, String[] args) {
		// TODO Auto-generated method stub
		ArrayList<String[]> print_list = parsing("src/test/test.mgf");

		for (String[] lines : print_list) {
			sp_manager.add(lines);
		}
	}

	@Override
	public String write(FeatureManager sp_manager, String[] args) {
		return null;

	}

	@Override
	public ArrayList<String[]> parsing(String file_name) {
		// TODO Auto-generated method stub
		ArrayList<String[]> lines_list = new ArrayList<>();
		// TODO Auto-generated method stub

		try {
			Reader reader = null;
			BufferedReader BR = null;

			String ext = file_name.substring(file_name.lastIndexOf("."));
			ArrayList<String> lines = new ArrayList<>();
			
			lines.add("MS/MS");

			if (ext.equals(".mgf")) {
				reader = new FileReader(file_name);
				BR = new BufferedReader(reader);
				String line = null;

				while (true) {
					line = BR.readLine();

					if (line == null)
						break;
					line = line.trim();

					if (line.equals("BEGIN IONS")) {
						lines = new ArrayList<>();
						lines.add(line);
						continue;
					} else if (line.equals("END IONS")) {
						lines.add(line);
						String[] lines_array = new String[lines.size()];
						lines_array = lines.toArray(lines_array);
						lines_list.add(lines_array);
						continue;
					}
					lines.add(line);
				}

				BR.close();
			} else if (ext.equals(".xml")) {

			} else if (ext.equals(".txt")) {

			}
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return lines_list;
	}

}