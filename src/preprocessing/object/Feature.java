package preprocessing.object;

import java.util.HashMap;
import java.util.List;

/**
 * Feature class has name and value of each feature.
 * Name of feature is only in AttrKey.
 * Some feature need to calculate additionally, in this situation, it is managed at child class.
 * Expressed child class is Spectrum and PSM class.
 * 
 * @author pjw23
 *
 */
public abstract class Feature {

	protected HashMap<AttrKey, Object> attr;
	
	public Feature() {
		attr = new HashMap<>();
	}
	
	/**
	 * The data type of return value need to cast, and all of them is already known.
	 * Spectrum class has type of SpectrumAttrKey.
	 * PSM class has type of PSMAttrKey.
	 * 
	 * @param key	Name of feature
	 * @return		Value of feature
	 */
	public Object getAttr(AttrKey key) {
		return attr.get(key);
	}
	
	
	/**
	 * Spectrum class has type of SpectrumAttrKey.
	 * PSM class has type of PSMAttrKey.
	 * 
	 * @param key		Name of feature
	 * @param value		Value of feature
	 */
	protected void putAttr(AttrKey key, Object value) {
		attr.put(key, value);
	}
	
	/**
	 * Some feature need to calculate with given data.
	 * And this function do these calculation and store feature.
	 * 
	 * @throws Exception
	 */
	public abstract void generateAttr() throws Exception;
	
	public abstract List<Peak> getPeakList();
	public abstract void setPeakList(List<Peak> peak_list);
}
