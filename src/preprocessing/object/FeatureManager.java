package preprocessing.object;

import java.util.ArrayList;
import java.util.List;

public interface FeatureManager {
	ArrayList<Feature[]> spectrum_list = new ArrayList<>();
	
	void add(String[] lines);
	Feature[] remove(int index);
	
	int size();
	
	List<Peak> getPeakList(int index);
	void setPeakList(int index, List<Peak> peak_list);
	
	void generateAttr();
}

