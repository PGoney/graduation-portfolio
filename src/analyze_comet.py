import sys
import os
import copy

from my_data import SearchedSpectrum

def has_same_element(sp_list):
    prev_peptide = None
    for sp in sp_list:
        if sp != None:
            if prev_peptide == None:
                prev_peptide = sp.peptide
            elif prev_peptide != sp.peptide:
                return False
    return True

if __name__ == '__main__':
    original_file_in = sys.argv[1]
    file_in_list = sys.argv[2:-1]
    file_out = sys.argv[-1]

    print("Input (original):\t%s" % os.path.basename(original_file_in))

    original_sp_dic = {}
    sp_dic = {}

    with open(original_file_in, 'r') as f:
        info_line = f.readline()
        spectrum_cnt = 0
        while True:
            line = f.readline()
            if not line: break

            split_line = line.split()

            title = split_line[0]
            index = int(split_line[1])
            scan_number = int(split_line[2])
            observed_mw = float(split_line[3])
            charge = int(split_line[4])
            calculated_mw = float(split_line[5])
            delta_mass = float(split_line[6])
            score = float(split_line[7])
            peptide = split_line[8]
            protein = [ split_line[9] ]

            sp = SearchedSpectrum(title=title, index=index, scan_number=scan_number \
                    , observed_mw=observed_mw, charge=charge, calculated_mw=calculated_mw \
                    , delta_mass=delta_mass, score=score, peptide=peptide, protein=protein)

            original_sp_dic[title] = sp

            spectrum_cnt += 1
            print("\rspectrum count:\t%d" % spectrum_cnt, end='')
        print('')
        print("--------------------------------")

    file_cnt = 0
    for file_in in file_in_list:
        match = []
        non_match = []
        TopN = []
        original = []

        original_sp_dic_copy = copy.deepcopy(original_sp_dic)

        with open(file_in, 'r') as f:
            print("Input (analyze) :\t%s" % os.path.basename(file_in))

            f.readline()

            spectrum_cnt = 0
            while True:
                line = f.readline()
                if not line: break

                split_line = line.split()

                title = split_line[0]
                index = int(split_line[1])
                scan_number = int(split_line[2])
                observed_mw = float(split_line[3])
                charge = int(split_line[4])
                calculated_mw = float(split_line[5])
                delta_mass = float(split_line[6])
                score = float(split_line[7])
                peptide = split_line[8]
                protein = [ split_line[9] ]

                sp = SearchedSpectrum(title=title, index=index, scan_number=scan_number \
                        , observed_mw=observed_mw, charge=charge, calculated_mw=calculated_mw \
                        , delta_mass=delta_mass, score=score, peptide=peptide, protein=protein)

                original_sp = original_sp_dic_copy.get(title)
                original_peptide = None
                if original_sp != None:
                    original_peptide = original_sp.peptide

                if original_peptide == None:
                    TopN.append(sp)
                else:
                    if original_peptide != peptide:
                        non_match.append(sp)
                    else:
                        match.append(sp)
                    del(original_sp_dic_copy[title])

                if sp_dic.get(title) == None:
                    sp_dic[title] = [ None for i in range(len(file_in_list)) ]
                sp_dic[title][file_cnt] = sp

                spectrum_cnt += 1
                print("\rspectrum count: %d" % (spectrum_cnt), end='')
            print('')
            original = original_sp_dic_copy.values()

            print("Match:    \t%d" % len(match))
            print("Non-match:\t%d" % len(non_match))
            print("TopN:     \t%d" % len(TopN))
            print("Original: \t%d" % len(original))
            print("--------------------------------")

            if not os.path.exists('./match/'):
                os.mkdir('./match')
            if not os.path.exists('./non_match/'):
                os.mkdir('./non_match')
            if not os.path.exists('./TopN/'):
                os.mkdir('./TopN')
            if not os.path.exists('./original/'):
                os.mkdir('./original/')

            base_name = os.path.basename(file_in)

            with open('./match/%s' % base_name, 'w') as f_match_in:
                f_match_in.write(info_line)
                for sp in match:
                    f_match_in.write(str(sp) + '\n')

            with open('./non_match/%s' % base_name, 'w') as f_non_match_in:
                f_non_match_in.write(info_line)
                for sp in non_match:
                    f_non_match_in.write(str(sp) + '\n')

            with open('./TopN/%s' % base_name, 'w') as f_TopN_in:
                f_TopN_in.write(info_line)
                for sp in TopN:
                    f_TopN_in.write(str(sp) + '\n')

            with open('./original/%s' % base_name, 'w') as f_original_in:
                f_original_in.write(info_line)
                for sp in original:
                    f_original_in.write(str(sp) + '\n')

            file_cnt += 1

    with open(file_out, 'w') as f:
        line = ''
        for file_in in file_in_list:
            line += '\t' + os.path.basename(file_in)
        f.write(line + '\n')

        for title in sp_dic.keys():
            line = title

            sp_list = sp_dic[title]
            for sp in sp_list:
                if sp == None:
                    line += '\t'
                else:
                    line += '\t' + sp.peptide + ", " + sp.get_protein()

            f.write(line + '\n')

    file_name = os.path.splitext(file_out)[0]
    ext_name = os.path.splitext(file_out)[1]

    with open(file_name + "_different_from_binning_20" + ext_name, 'w') as f:
        line = ''
        for file_in in file_in_list:
            line += '\t' + os.path.basename(file_in)
        f.write(line + '\n')

        cnt = 0
        for title in sp_dic.keys():
            line = title

            sp_list = sp_dic[title]
            if has_same_element(sp_list) == False:
                for sp in sp_list:
                    if sp == None:
                        line += '\t'
                    else:
                        line += '\t' + sp.peptide + ", " + sp.get_protein()
                f.write(line + '\n')

                cnt += 1
                print("\rIdentified different spectrum from binning 20:\t%d" % cnt, end='')
        print('')

    with open(file_name + "_different_from_binning_50" + ext_name, 'w') as f:
        line = ''
        for file_in in file_in_list:
            line += '\t' + os.path.basename(file_in)
        f.write(line + '\n')

        cnt = 0
        for title in sp_dic.keys():
            line = title

            sp_list = sp_dic[title]
            sp_list[3] = None
            if has_same_element(sp_list) == False:
                for sp in sp_list:
                    if sp == None:
                        line += '\t'
                    else:
                        line += '\t' + sp.peptide + ", " + sp.get_protein()
                f.write(line + '\n')

                cnt += 1
                print("\rIdentified different spectrum from binning 50:\t%d" % cnt, end='')
        print('')
